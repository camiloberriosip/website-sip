import { Routes, RouterModule } from '@angular/router';

// Route for content layout with sidebar, navbar and footer
export const Full_ROUTES: Routes = [
  {
    path: 'changelog',
    loadChildren: () => import('../../changelog/changelog.module').then(m => m.ChangeLogModule)
  },
  {
    path: 'user',
    loadChildren: () => import('../../pages/full-layout-page/full-pages.module').then(m => m.FullPagesModule)
  },
  {
    path: 'calendar',
    loadChildren: () => import('../../pages/full-layout-page/calendar/calendar.module').then(m => m.CalendarsModule)
  },
  {
    path: 'files-available',
    loadChildren: () => import('../../pages/full-layout-page/files-available/files-available.module').then(m => m.FilesAvailableModule)
  }
];
