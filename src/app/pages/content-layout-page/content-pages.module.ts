import { ErrorPageComponent } from './error/error-page.component';
import { ForgotPasswordPageComponent } from './forgot-password/forgot-password-page.component';
import { RegisterPageComponent } from './register/register-page.component';
import { LoginPageComponent } from './login/login-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ContentPagesRoutingModule } from './content-pages-routing.module';
import { SitePublicComponent } from './site-public/site-public.component';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
    CommonModule,
    ContentPagesRoutingModule,
    FormsModule,
    NgxLoadingModule.forRoot({})
  ],
  declarations: [
    SitePublicComponent, LoginPageComponent, RegisterPageComponent, ForgotPasswordPageComponent, ErrorPageComponent
  ]
})
export class ContentPagesModule { }
