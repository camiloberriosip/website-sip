import { ErrorPageComponent } from './error/error-page.component';
import { ForgotPasswordPageComponent } from './forgot-password/forgot-password-page.component';
import { RegisterPageComponent } from './register/register-page.component';
import { LoginPageComponent } from './login/login-page.component';
import { SitePublicComponent } from './site-public/site-public.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'public',
     component: SitePublicComponent,
    data: {
      title: 'Site public'
    },
  },
  {
    path: 'login',
     component: LoginPageComponent,
    data: {
      title: 'Login'
    },
  },
  {
    path: 'register',
     component: RegisterPageComponent,
    data: {
      title: 'Register'
    },
  },
  {
    path: 'forgotpassword',
     component: ForgotPasswordPageComponent,
    data: {
      title: 'Forgot Password'
    },
  },
  {
    path: 'error',
     component: ErrorPageComponent,
    data: {
      title: 'Error Page'
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContentPagesRoutingModule { }
