import { FilesAvailableRoutingModule } from './files-available-routing.module';
import { FilesAvailableComponent } from './files-available.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [FilesAvailableComponent],
  imports: [
    CommonModule,
    FilesAvailableRoutingModule
  ]
})
export class FilesAvailableModule { }
