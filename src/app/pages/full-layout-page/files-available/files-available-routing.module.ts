import { FilesAvailableComponent } from './files-available.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
     component: FilesAvailableComponent,
    data: {
      title: 'My Files'
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FilesAvailableRoutingModule { }
export const routedComponents = [FilesAvailableComponent];

